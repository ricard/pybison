#!/usr/bin/env python

import readline

import calc

parser = calc.Parser(verbose=0, keepfiles=0)
parser.run()
