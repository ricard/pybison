//@+leo-ver=4
//@+node:@file src/c/bisondynlib-linux.c
//@@language c
/*
 * Linux-specific dynamic library manipulation routines
 */

#include <stdio.h>
#include "bisondynlib.h"
#include <dlfcn.h>

void *bisondynlib_open(char *filename)
{
    void *handle;

    dlerror();
    handle = dlopen(filename, (RTLD_NOW|RTLD_GLOBAL));
    return handle;
}

int bisondynlib_close(void *handle)
{
    return dlclose(handle);
}

char *bisondynlib_err()
{
    return dlerror();
}

char *bisondynlib_lookup_hash(void *handle)
{
    char **hash;
    dlerror();
    hash = dlsym(handle, "rules_hash");
    /*
    printf("bisondynlib_lookup_hash: hash=%s\n", *hash);
    */
    return *hash;
}

PyObject *bisondynlib_run(void *handle, PyObject *parser, void *cb, void *in, int debug)
{
    void (*pparser)(PyObject *, void *, void *, int);
    //PyObject *result;

    //printf("bisondynlib_run: looking up parser\n");
    pparser = bisondynlib_lookup_parser(handle);
    //printf("bisondynlib_run: calling parser, py_input=0x%lx\n", in);
    (*pparser)(parser, cb, in, debug);
    //printf("bisondynlib_run: back from parser\n");
    //return result;
    Py_INCREF(Py_None);
    return Py_None;

}

/*
 * function(void *) returns a pointer to a function(PyObject *, char *) returning PyObject*
 */
void (*bisondynlib_lookup_parser(void *handle))(PyObject *, void *, void *, int)
{
    dlerror();
    return dlsym(handle, "do_parse");
}

PyObject * bisondynlib_run_lexer(void *handle, PyObject *parser, void *in, int restart, int debug, _YYLTYPE *yylloc)
{
	const char* token = NULL;
	lexer_fn *plexer;
	PyObject * tokenValue = NULL;

    //printf("bisondynlib_run: looking up lexer\n");
    plexer = bisondynlib_lookup_lexer(handle);
    //printf("bisondynlib_run: calling lexer\n");

    token = (*plexer)(parser, in, restart, &tokenValue, yylloc, debug);
	if(tokenValue)
	{
		PyObject_SetAttrString(parser, "token", tokenValue);
		Py_DECREF(tokenValue);
	}
	else
		PyObject_SetAttrString(parser, "token", PyString_FromString(""));
    //printf("bisondynlib_run: back from parser\n");
    //return result;
    return PyString_FromString(token);

}

/*
 * function(void *) returns a pointer to a function(PyObject *, char *) returning PyObject*
 */
//const char *(*bisondynlib_lookup_lexer(void *handle))(int, PyObject **, void *, int)
lexer_fn *bisondynlib_lookup_lexer(void *handle)
{
    dlerror();
    return dlsym(handle, "do_get_next_token");
}


/*
 * Runs the compiler commands which build the parser/lexer into a shared lib
 */
 /*
int bisondynlib_build(char *libName, char *pyincdir)
{
    char buf[1024];
    sprintf(buf, "gcc -fPIC -shared -I%s tmp.bison.c tmp.lex.c -o %s", pyincdir, libName);
    printf("Running linux build command: %s\n", buf);
    system(buf);
    return 0;
}
*/

//@-node:@file src/c/bisondynlib-linux.c
//@-leo
