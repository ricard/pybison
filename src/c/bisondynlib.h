//@+leo-ver=4
//@+node:@file src/c/bisondynlib.h
//@@language c
/*
 * common interface to dynamic library routines
 */

#include <stdio.h>
#include "Python.h"

typedef struct _YYLTYPE {
    int first_line;
    int first_column;
    int last_line;
    int last_column;
    int start_pos;
    int end_pos;
} _YYLTYPE;

typedef const char *(lexer_fn)(void *, void *, int, PyObject **, _YYLTYPE *, int);

void *bisondynlib_open(char *filename);
int bisondynlib_close(void *handle);
char *bisondynlib_err(void);

//PyObject *(*bisondynlib_lookup_parser(void *handle))(PyObject *, void *, void *, int);
void (*bisondynlib_lookup_parser(void *handle))(PyObject *, void *, void *, int);
//const char *(*bisondynlib_lookup_lexer(void *handle))(int, PyObject **, void *, int);
lexer_fn* bisondynlib_lookup_lexer(void *handle);

char *bisondynlib_lookup_hash(void *handle);

PyObject *bisondynlib_run(void *handle, PyObject *parser, void *cb, void *in, int debug);
PyObject *bisondynlib_run_lexer(void *handle, PyObject *parser, void *in, int restart, int debug, _YYLTYPE *yylloc);

/*
int bisondynlib_build(char *libName, char *pyincdir);
*/
//@-node:@file src/c/bisondynlib.h
//@-leo
